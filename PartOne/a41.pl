% Problem #2, "Imaginary friends", Keith king
% Each man (mr so-and-so) has a imaginary friend and has an adventure.
ifriend(grizzly_bear).
ifriend(moose).
ifriend(seal).
ifriend(zebra).

mr(joanne).
mr(lou).
mr(ralph).
mr(winnie).

adventure(circus).
adventure(rock_band).
adventure(spaceship).
adventure(train).

solve :-
    ifriend(JoanneIfriend), ifriend(LouIfriend), ifriend(RalphIfriend), ifriend(WinnieIfriend),
    all_different([JoanneIfriend, LouIfriend, RalphIfriend, WinnieIfriend]),

    adventure(JoanneAdventure), adventure(LouAdventure),
    adventure(RalphAdventure), adventure(WinnieAdventure),
    all_different([JoanneAdventure, LouAdventure, RalphAdventure, WinnieAdventure]),

    Triples = [ [joanne, JoanneIfriend, JoanneAdventure],
                [lou, LouIfriend, LouAdventure],
                [ralph, RalphIfriend, RalphAdventure],
                [winnie, WinnieIfriend, WinnieAdventure] ],

    % 1.
	 \+ member([joanne, seal, _], Triples),
	\+ member([lou, seal, _], Triples),
	\+ member([_, seal, spaceship], Triples),
	\+ member([_, seal, train], Triples),

    % 2. 
    \+ member([joanne, grizzly_bear, _], Triples),
    member([joanne, _, circus], Triples),

    % 3. 
    member([winnie, zebra, _], Triples),
	
	% 4. 
    \+ member([_, grizzly_bear, spaceship], Triples),


    tell(joanne, JoanneIfriend, JoanneAdventure),
    tell(lou, LouIfriend, LouAdventure),
    tell(ralph, RalphIfriend, RalphAdventure),
    tell(winnie, WinnieIfriend, WinnieAdventure).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z) :-
    write('Mr. '), write(X), write(' has an imaginary friend '), write(Y),
    write(' which has an adventure '), write(Z), write('.'), nl.


