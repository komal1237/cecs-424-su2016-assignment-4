% Problem #2, "start tricked", Keith king
% Each person (on a specific day) saw something in the neighborhood and explained them.

person(barrada).
person(gort).
person(klatu).
person(nikto).

day(tuesday).
day(wednesday).
day(thursday).
day(friday).

explained(balloon).
explained(clothesline).
explained(frisbee).
explained(water_tower).

solve :-
    day(BarradaDay), day(GortDay), day(KlatuDay), day(NiktoDay),
    all_different([BarradaDay, GortDay, KlatuDay, NiktoDay]),
    
    explained(BarradaExplained), explained(GortExplained),
    explained(KlatuExplained), explained(NiktoExplained),
    all_different([BarradaExplained, GortExplained, KlatuExplained, NiktoExplained]),

    Triples = [ [barrada, BarradaDay, BarradaExplained],
                [gort, GortDay, GortExplained],
                [klatu, KlatuDay, KlatuExplained],
                [nikto, NiktoDay, NiktoExplained] ],
   
    % 1. Mr. Klatu made the sighting earlier in the week than the one who saw the balloon, but at the same point later in the week than the one who spotted the Frisbee(who isn't Mr. Gort).
    \+ member([klatu, wednesday, _], Triples),
       member([_,tuesday,balloon], Triples),
	member([_,thursday,frizbee], Triples),
	\+member([gort, _, frizbee], Triples);
    
    \+ member([klatu, thursday, _], Triples),
	member([_, wednesday, balloon], Triple),
	member([_, friday, frisbee], Triples)
    \+member([gort, _, frizbee], Triples),
   
    
    % 2. friday's sighting was made by either Mr. Barrada or the one who saw the clothesline(or both).
     member([barrada, friday, _], Triples);
     member([_, friday, clothesline], Triples);
     member([barrada, friday, clothesline], Triples),
	
    
    % 3. Mr. nikto did not make his sighting on tuesday.
    \+ member([nikto, tuesday, _], Triples),
    
    % 4. Mr. Klatu's object did not come out to be water tower.
    \+ member([klatu, _, water_tower], Triples),
    
        
    tell(barrada, BarradaDay, BarradaExplained),
    tell(gort, GortDay, GortExplained),
    tell(klatu, KlatuDay, KlatuExplained),
    tell(nikto, NiktoDay, NiktoExplained).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y, Z) :-
    write('name of the person is '), write(X), write(' and on '), write(Y),
    write(' saw an object which they explained as'), write(Z), write('.'), nl.