:- dynamic i_am_at/1, at/2, holding/1.
:- retractall(at(_, _)), retractall(i_am_at(_)), retractall(alive(_)).

i_am_at(hell).

path(hell, n, heaven).
path(hell, s, castle).
path(castle, e, treasure):- at(bomb, in_hand).
path(treasure, d, tunnel).

path(tunnel, w, kingdom):- at(key, in_hand).
path(tunnel, w, kingdom):- write('the door is locked'),
        nl.

at(key, treasure).
at(dragon, treature).
at(bomb, castle).

/*dragon that you will find in castle is alive*/

alive(dragon).


/* These rules describe how to pick up an object. */


take(X) :-
        holding(X),
        write('You''re already holding it!'),
        !, nl.

take(X) :-
        i_am_at(Place),
        at(X, Place),
        retract(at(X, Place)),
        assert(holding(X)),
        write('OK.'),
        !, nl.

take(_) :-
        write('I don''t see it here.'),
        nl.


/* These rules describe how to put down an object. */

drop(X) :-
        holding(X),
        i_am_at(Place),
        retract(holding(X)),
        assert(at(X, Place)),
        write('OK.'),
        !, nl.

drop(_) :-
        write('You aren''t holding it!'),
        nl.


/* These rules define the direction letters as calls to go/1. */

n :- go(n).

s :- go(s).

e :- go(e).

w :- go(w).

d:- go(d).



/* This rule tells how to move in a given direction. */

go(Direction) :-
        i_am_at(Here),
        path(Here, Direction, There),
        retract(i_am_at(Here)),
        assert(i_am_at(There)),
        !, look.

go(_) :-
        write('You can''t go that way.').


/* This rule tells how to look about you. */

look :-
        i_am_at(Place),
        describe(Place),
        nl,
        notice_objects_at(Place),
        nl.


/* These rules set up a loop to mention all the objects
   in your vicinity. */

notice_objects_at(Place) :-
        at(X, Place),
        write('There is a '), write(X), write(' here.'), nl,
        fail.

notice_objects_at(_).

/* This rule tells how to die. */

kill :-
        i_am_at(hell),
        write('There is no one to kill, go ahead '), nl.
        

kill :-
        i_am_at(castle),
        write('you see a bomb here, it might help you later in the game'), nl,
        write('may be something dangerous.').

kill :-
        i_am_at(treature),
        at(bomb, in_hand),
        retract(alive(dragon)),
        write('You have a free refill of the bomb keep killing it till it dies'), nl,
        write('I think you have killed it, and have used a lot of bombs though.'),
        nl, !.

kill :-
        i_am_at(treature),
        write('the dragon is killed, good job'), nl.

kill :-
        write('Lets go ahead and see how you can win the game'), nl.



die :-
        finish.


/* Under UNIX, the "halt." command quits Prolog but does not
   remove the output window. On a PC, however, the window
   disappears before the final output can be seen. Hence this
   routine requests the user to perform the final "halt." */

finish :-
        nl,
        write('The game is over. Please enter the halt. command.'),
        nl.


/* This rule just writes out game instructions. */

instructions :-
        nl,
        write('Enter commands using standard Prolog syntax.'), nl,
        write('Available commands are:'), nl,
        write('start.             -- to start the game.'), nl,
        write('n.  s.  e.  w. d.     -- to go in that direction.'), nl,
        write('take(Object).      -- to pick up an object.'), nl,
        write('drop(Object).      -- to put down an object.'), nl,
        write('look.              -- to look around you again.'), nl,
        write('instructions.      -- to see this message again.'), nl,
        write('halt.              -- to end the game and quit.'), nl,
        nl,
	write('kill.			--to kill someone(dragon).'), nl, nl.

/* This rule prints out instructions and tells where you are. */

start :-
        instructions,
        look.


/* These rules describe the various rooms.  Depending on
   circumstances, a room may have more than one description. */

describe(X) :- write('You are in '), write(X), nl.

describe(hell) :-
        
        write('you are in hell, and it does not seem to be a nice place'), nl,
	write('Towards the south is a castle which may take you somewhere better'),nl,
	write('towards the north is a heaven, seems to be fascinating by the name'),nl.
        

describe(heaven) :-
        write('It is a trap, go back, you see nothing here'), nl,
        write('towards the south is the Hell where you came from, nothing else'), nl.
        

describe(castle) :-
        at(bomb, in_hand),
	write('You are holding a bomb now, it might help you later, goodluck'), nl,
        write('There is a room named treasure towards the east'), nl.
        

describe(treasure) :-
	alive(dragon),
        write('You are in a huge room, but you hardly see anything here'), nl,
	write('the dragon seems to be huge and powerful, you need to kill it to win the game'), nl.
        

describe(tunnel) :-
	at(key, in_hand),
        write('This is nothing but compact passage.'), nl,
	write('towards the west is the kingdom'), nl.

describe(kingdom) :-
        write('You are in a gold huge hall, which is the final desination'), nl,
        write('You have now become the king by reaching here'), nl,
        write('Congratulations.! You have won the game.'), nl.

